<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/wish', function () {
    return view('wish');
});

Route::get('/toko', function () {
    return view('toko');
});

Route::get('/tim', function () {
    return view('tim');
});


Route::get('/home', 'BukuController@index')->name('home');

Route::resource('wislist', 'wiscontroler');
Route::resource('toko', 'TokoController');
Route::resource('buku', 'BukuController');

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();