<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
        
    protected $table = 'toko';
    protected $fillable = ['nama_toko','alamat','updated_at','created_at'];
    public $timestamps = false;

}
