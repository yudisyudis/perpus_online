<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class wis extends Model
{
    protected $table = 'wisfix';
    protected $fillable = ['judul_buku', 'poster', 'user_id'];

    // public function User()
    // {
    //     // kasih belongsto karena di table ada foreign key nya
    //     return $this->belongsTo('App\User');
    // }
    // public $timestamps = false;
}
