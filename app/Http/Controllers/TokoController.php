<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Toko;

class TokoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except('store');
    }
    public function index()
    {
        $listtoko = toko::all();
        return view('toko', compact('listtoko'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('toko.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $request->validate([
            'nama_toko' => 'required',
            'alamat' => 'required',
        ]);

        $listtoko = new toko;
        $listtoko->nama_toko = $request->nama_toko;
        $listtoko->alamat = $request->alamat;
        $listtoko->user_id =  auth()->user()->id;
        $listtoko->save();
        return redirect('/toko')->with('success', 'Berhasil menambahkan toko');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $catchedtoko = toko::findOrFail($id);

        return view('toko.show', compact('catchedtoko'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $listtoko = DB::table('toko')->get();
        $catchedtoko = toko::findOrFail($id);
        return view('toko.edit', compact('catchedtoko'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_toko' => 'required',
            'alamat' => 'required',
        ]);

        $editedtoko = toko::find($id);
        $editedtoko->nama_toko = $request->nama_toko;
        $editedtoko->alamat = $request->alamat;
        $editedtoko->update();
        return redirect('/toko');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $listtoko = toko::find($id);
        $listtoko->delete();

        return redirect('/toko');
    }
}
