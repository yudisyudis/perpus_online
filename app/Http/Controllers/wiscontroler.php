<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use DB;
use App\wis;
use File;


class wiscontroler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wis = wis::all();
        return view('wis.index', compact('wis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('wis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // proses validasi inputan
        $request->validate([
            'judul' => 'required',
            'poster' => 'required',
            'penerbit' => 'required',
            // validasi upload gambar
            // maksimal poster 2mb
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $poster = time() . '.' . $request->poster->extension();
        // memasukkan gambar ke folder target
        $request->poster->move(public_path('poster'), $poster);
        $wis = new wis;
        $wis->judul_buku = $request->judul;
        $wis->poster = $poster;
        $wis->user_id = Auth::id();
        $wis->penerbit = $request->penerbit;
        $wis->save();
        Alert::success('Tambah Data Berhasil', 'Success Message');

        return redirect('/wislist');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $wis = wis::findOrFail($id);
        return view('wis.show', compact('wis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wis = wis::findOrFail($id);
        return view('wis.edit', compact('wis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'penerbit' => 'required',
            'poster' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $wis = wis::find($id);
        if ($request->has('poster')) {
            $poster = time() . '.' . $request->poster->extension();
            // memasukkan gambar ke folder target
            $request->poster->move(public_path('poster'), $poster);
            // proses update
            $wis->judul_buku = $request->judul;
            $wis->poster = $poster;
            $wis->user_id = Auth::id();
            $wis->penerbit = $request->penerbit;
            $wis->update();
            Alert::success('Berhasil', 'Data Berhasil Di Perbarui');
            return redirect('/wislist');
        } else {
            $wis->judul_buku = $request->judul;
            $wis->user_id = Auth::id();
            $wis->penerbit = $request->penerbit;
            // $film->poster = $poster;
            $wis->update();
            Alert::success('Berhasil', 'Data Berhasil Di Perbarui');
            return redirect('/wislist');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wis = wis::find($id);
        $patch = "poster/";
        File::delete($patch . $wis->poster);
        $wis->delete();
        Alert::success('Berhasil', 'Data Berhasil Di Hapus');
        return redirect('/wislist');
    }
}
