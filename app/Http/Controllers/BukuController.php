<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use DB;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Book::all();
        return view('home', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/buku/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nama = $request -> gambar;
        $namaFile = $nama -> getClientOriginalName();
 
        $upload = DB::table('books')->insert([
         'nama' => $request['nama'],
         'deskripsi' => $request['deskripsi'],
         'gambar' => $namaFile,
        ]);
 
        $nama -> move (public_path().'/img', $namaFile);
 

        return redirect('/buku');





    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data1 = DB::table('books')->where('id',$id)->first();        
        return view('/buku/show', compact('data1'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data2 = Book::where('id', $id)->first();
        return view('/buku/edit', compact('data2'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nama = $request -> gambar;
        $namaFile = $nama -> getClientOriginalName();

        $update = DB::table('books')

              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'deskripsi' => $request['deskripsi'],
                  'gambar' => $namaFile
                ]);
                $nama -> move (public_path().'/images', $namaFile);
        return redirect('/buku')->with('success', 'berhasil update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Book::destroy($id);
        return redirect('/buku');

    }
}
