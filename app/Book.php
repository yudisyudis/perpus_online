<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
    public $timestamps = false;
    protected $fillable = ['nama', 'deskripsi', 'gambar'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
