@extends('master')
@section('content')
<div class="container">
<form action="/wislist/{{$wis->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="mb-3">
      <label class="form-label" for="judul" style="color: white">Judul Buku</label>
      <input type="text" name="judul"class="form-control" id="judul" value="{{$wis->judul_buku}}">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
      <label class="form-label" for="judul" style="color: white">Penerbit</label>
      <input type="text" name="penerbit"class="form-control" id="penerbit" value="{{$wis->penerbit}}">
    </div>
    @error('penerbit')
    
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="mb-3">
      <label class="form-label" for="poster" style="color: white">Poster</label> <br>
     <input type="file" name="poster" class="form-label" id="poster">
    </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
    
@endsection