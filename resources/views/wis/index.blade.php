@extends('master')

@section('content')
<div class="container">
  <div class="row">
    @auth
    {{-- tombol tambah akan tampil ketika sudah login --}}
    <a class="btn btn-light mb-2" href="/wislist/create">Tambah Wistlist</a>
    @endauth
  <table class="table table-dark table-hover">
    <caption>List of Wistlist</caption>
      <thead class="text-center">
        <tr >
          <th scope="col">No</th>
          <th scope="col">Judul Buku</th>
          <th scope="col">Penerbit</th>
          <th scope="col">Option</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($wis  as $key=>$item)
        <tr>
          <td style="text-align: center">{{$key + 1}}</td>
          <td>{{$item->judul_buku}}</td>
          <td>{{$item->penerbit}}</td>
          <td style="text-align: center">
            <form action="/wislist/{{$item->id}}" method="post">
             @method('DELETE')
             @csrf
             <a class="btn btn-info text-light mx-1" href="/wislist/{{$item->id}}"><i class="fas fa-eye"></i></a>
             {{-- button edit --}}
             <a class="btn btn-outline-warning" href="/wislist/{{$item->id}}/edit"><i class="far fa-edit"></i></a>
             <button input type="submit" class="btn btn-outline-danger mx-1" value="delete">
              <i class="far fa-trash-alt"></i></button>
           </form>
         </td>
        </tr>
        @empty
        <tr>
          <div class="alert alert-info"> Data Masih Kosong</div>
        </tr>
        @endforelse
      </tbody>
  </table>
    
  </div>
</div>

@endsection