@extends('master')

@section('content')

<div class="container" style="padding-left: 20px">
<form action="/wislist" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
      <label class="form-label" for="judul" style="color: white">Judul Buku</label>
      <input type="text" name="judul"class="form-control" id="judul">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
      <label class="form-label" for="judul" style="color: white">Penerbit</label>
      <input type="text" name="penerbit"class="form-control" id="penerbit">
    </div>
    @error('penerbit')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="mb-3">
      <label class="form-label" for="poster" style="color: white">Gambar Buku</label> <br>
     <input type="file" name="poster" class="form-label" id="poster">
    </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-success">Submit</button>
  </form>
</div>
@endsection