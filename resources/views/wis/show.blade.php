@extends('master')

@section('content')

<div class="container" style="padding-left: 35%">
<div class="card" style="width: 18rem;">
  <img src="{{asset('poster/'.$wis->poster)}}" class="card-img-top" alt="ini adalah gambar">
  <div class="card-body">
    <p>Poster Wishbook</p>
    <h2 class="card-title">{{$wis->judul_buku}}</h2>
    <p>Penerbit : {{$wis->penerbit}}</p>
    <a class="btn btn-success"  href="/wislist">Kembali</a>
   
  </div>
</div>
</div>

@endsection
