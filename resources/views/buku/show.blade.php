@extends('master')

@section('content')
<div class="card-body">
    <div class="post">
        <div class="col d-flex justify-content-center">

            <div class="user-block">
                <h5>{{$data1->nama}}</h5>

                <img src="{{asset('/img/'.$data1->gambar)}}" alt="gambar" width=25%> 
                <h3>{{$data1->deskripsi}}</h3> 
            </div>                  
        </div>
    </div>
</div>

@endsection

