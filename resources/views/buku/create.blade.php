@extends('master')

@section('content')
<div class="container h-50">
    <div class='form'>
        <form action="{{route('buku.index')}}" method='POST' enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="nama"><h5>Nama Buku</h5></label>
                <input type="text" class="form-control" id="nama" name='nama' placeholder="Nama Buku">
            </div>

            <div class="form-group">
                <label for="deskripsi"><h5>Deskripsi</h5></label>
                <textarea class="form-control" id="deskripsi" name='deskripsi' rows="5"></textarea>
            </div>

            <div class="form-group">
                <label for="gambar"><h5>Gambar</h5></label>
                <br>
                <input id="gambar" name="gambar" type="file" class="file" data-browse-on-zone-click="true">
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </form>
    </div>
</div>

    
@endsection