@extends('master')

@section('create')
<a href="/toko/create" style="float: right"><button type="button" class="btn btn-light mt-1 mb-2" ><i class="fa fa-plus" aria-hidden="true"></i>
    Menambahkan toko</button></a>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('images/avatar4.png')}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

                <p class="text-muted text-center">Uploader</p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <a class="card-title" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
<div class="container">

<a class="btn btn-light mb-2" href="/toko/create">Menambahkan Toko</a>
      <table class="table table-bordered table-dark">
        <thead>
            @if (!empty($listtoko[0]))
            <tr>
            <th style="width: 50px">#</th>
            <th style="width: 400px">Nama Toko</th>
            <th>Alamat</th>
            <th style="width: 200px">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($listtoko as $indexArr => $toko)
              <tr>
                  <td>{{$indexArr + 1 }}</td>
                  <td>{{$toko -> nama_toko }}</td>
                  <td>{{$toko -> alamat }}</td>
                  <td style="display: flex;">
                      <a href="/toko/{{$toko->id}}" class="btn btn-info text-light mx-1"><i class="fas fa-eye"></i></a>
                      <a href="/toko/{{$toko->id}}/edit" class="btn btn-warning mx-1"><i class="far fa-edit"></i></a>
                      <form action="/toko/{{$toko->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button input type="submit" class="btn btn-outline-danger mx-1" value="delete">
                        <i class="far fa-trash-alt"></i></button>

                      </form>
                  </td>
              </tr>
          @endforeach
          @else
          <tr>
            <td colspan="5" align="center">No toko Data</td> 
          </tr>
          @endif
        </tbody>
      </table>
    </div>
  </div>
@endsection