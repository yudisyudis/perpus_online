@extends('master')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('images/avatar4.png')}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

                <p class="text-muted text-center">Software Engineer</p>

                <a href="{{route('buku.create')}}" class="btn btn-primary btn-block"><b>Tambah Buku</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <a class="card-title" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                  <h3>Koleksi Buku</h3>
              </div><!-- /.card-header -->
              <div class="card-body">
                <!-- Post -->
                @foreach($data as $key => $data)
                <div class="post">
                  <div class="user-block">
                  <img src="{{asset('/img/'.$data->gambar)}}" alt="gambar" width=25%>                    
                  <div class="username">
                      <a href="{{route('buku.show', ['buku' => $data->id])}}">{{$data->nama}}</a>
                  </div>
                  <p>{{$data->deskripsi}}</p>
                  <!-- /.user-block -->

                </div>
                <a href="buku/{{$data->id}}/edit" button class="w3-button w3-white w3-border w3-border-red w3-round-large">Edit</button>
                <form action="{{route('buku.destroy', ['buku' => $data->id])}}" method="post" style = display:inline-block;>
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-link">
                </form>
                @endforeach






                <!-- /.post -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@endsection
