@extends('master')
@section('content')
    <div class="card" style="margin-top:0px; width: 75rem; height:15rem; margin-left:30px">
        <div class="card-header font-weight-bold text-center">
            <h1>Tim Kami</h1>
        </div>

        <div class="container " style="margin-bottom: 250px; margin-left:50px; padding: 0px 25px 0px 25px">
            <div class="row">
                <div class="col-sm">
                    <img class="ml-lg-4" src="{{ asset('img/profile-tim.jpg') }}" width="30%">
                    <h6 class="">Muhamad Firmansyah </h6>
                    <p class="ml-lg-4">"Web Developer"</p>
                </div>
                <div class="col-sm">
                    <img class="ml-lg-3" src="{{ asset('img/profile-tim.jpg') }}" width="30%">
                    <h6 class="ml-md-3">Sahadat Islam</h6>
                    <p class="ml-lg-3">"Web Developer"</p>
                </div>
                <div class="col-sm">
                    <img class="ml-lg-4" src="{{ asset('img/profile-tim.jpg') }}" width="30%">
                    <h6 class="ml-md-3">Yudisthira Iriana Putra</h6>
                    <p class="ml-md-4">"Web Developer"</p>
                </div>

            </div>
        </div>
    </div>
@endsection