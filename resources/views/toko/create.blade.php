@extends('master')



@section('content')

<div class="container">
    
    <form role="form" action="/toko" method="POST">
        @csrf
        <div class="form-group row">
        <label for="inputNama" class="col-sm-2 col-form-label"><h4 class="text-light">Rekomendasi :</h4></label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="inputNama" name="nama_toko" placeholder="Nama Toko" >
            @error('nama_toko')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        
    </div>
   

    <div class="form-group row">
        <label for="inputalamat" class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
            <textarea type="textarea" class="form-control" id="inputAlamat" name="alamat" placeholder="Alamat Toko" rows="5" cols="50"></textarea>
            @error('alamat')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        
    </div>

    <button class="btn btn-info"  style="float: right"><i class="fa fa-plus add" aria-hidden="true" id="add"></i> Tambahkan</button>
    </form>
    
<a href="/toko"  style="float:left"><button type="button" class="btn btn-info mt-1 mb-2" ><i class="fa fa-angle-left" aria-hidden="true"></i>
    Back</button></a><br><br>
</div>

@endsection
