@extends('master')

@section('back')
<a href="/toko"><button type="button" class="btn btn-light" ><i class="fa fa-angle-left" aria-hidden="true"></i>
    Back</button></a><br>
@endsection

@section('content')

<div class="container">
<h3>Data</h3>
<table class="table table-dark table-hover">
    <tr>
        <td>Data</td>
        <td>Keterangan</td>
    </tr> 
    <tr>
        
        <td>ID</td>
        <td>{{$catchedtoko->id}}</td>
    </tr> 
    <tr>

        <td style="width: 200px">Nama Toko</td>
        <td>{{$catchedtoko->nama_toko}}</td>
    </tr> 
    <tr>
        <td style="width: 400px">Alamat</td>
        <td>{{$catchedtoko->alamat}}</td>
    </tr> 
</table>
</div>



@endsection
