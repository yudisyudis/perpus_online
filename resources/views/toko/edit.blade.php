@extends('master')

@section('back')
<a href="/toko"><button type="button" class="btn btn-primary" ><i class="fa fa-times" aria-hidden="true"></i> Cancel</button></a><br>
@endsection

@section('content')

<div class="container">
    
    <form role="form" action="/toko/{{$catchedtoko->id}}" method="POST">
        @csrf
        @method('PUT')
    <div class="form-group row">
        <label for="inputNama" class="col-sm-2 col-form-label">Nama Toko:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="inputNama" name="nama_toko" value="{{old('nama_toko', $catchedtoko->nama_toko)}}" placeholder="Nama_toko" >
            @error('Nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        
        
    </div>

    <div class="form-group row">
        <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat Toko:</label>
        <div class="col-sm-10">
            <textarea class="form-control" id="inputAlamat" name="alamat" placeholder="Alamat" rows="5" cols="50">{{old('toko', $catchedtoko->alamat)}}</textarea>
            @error('alamat')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        
    </div>

    <button type="submit" class="btn btn-info add"><i class="fa fa-check" aria-hidden="true" id="add"></i> Tambahkan</button>
    </form>
</div>

@endsection
